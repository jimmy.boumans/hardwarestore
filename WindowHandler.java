public class WindowHandler extends WindowAdapter {
    HardwareStore hardwareStore;

    public WindowHandler(HardwareStore store) {
        hardwareStore = store;
    }

    public void windowClosing(WindowEvent e) {
        hardwareStore.cleanup();
    }
}