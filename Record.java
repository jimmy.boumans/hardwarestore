import javax.swing.JOptionPane;
import java.io.*;
import javax.swing.*;
import java.util.*;

/*
** The Record class purpose is to read and write records to a randomaccess file.
*/
public class Record {
   private int recordId;
   private int quantity;
   private String toolType = "";
   private String brandName = "";
   private String toolDesc  = "";
   private String partNum = "";
   private String cost = "";
   private String recordTokens[];
   private boolean debugState = false;
   private long filePos;
   private long fileLen;

   /*
   **  ReadRec() Reads a record from the specified RandomAccessFile.
   */
   public void ReadRec(RandomAccessFile file) throws IOException {
      char f[] = new char[585], ch;
      StringTokenizer tokens;
      String str = "", str2 = "";
      StringBuffer buf1  = new StringBuffer("");
      int counter = 0, loopCtl = 585, len = 0 ;
      long remm = fileLen - filePos ;

      sysPrint("ReadRec() 1a: Remaining bytes is " + remm);
      sysPrint("ReadRec() 1b: Reading ints");
      recordId = file.readInt();
      sysPrint("ReadRec() 1c: recordId  is " +  recordId) ;
      quantity = file.readInt();
      sysPrint("ReadRec() 2: Reading string") ;
      while (counter < loopCtl) {
         ch =  file.readChar();
         str = str + ch;
         len = str.length();
         if (counter > 4) {
            str2 = str.substring(len - 4 ,len - 1) ;
            if (str2.equals( ";;;")) {
                break ;
            }
         }
         counter++ ;
      }
      sysPrint("ReadRec() 3a: str is " + str);
      sysPrint("ReadRec() 3b: Reading string. counter =s " + counter);
      sysPrint("ReadRec() 4: The value of str is " + str);
      tokens = new StringTokenizer(str, ";;");
      recordTokens = new String[7];
      if (tokens.countTokens() >= 4) {
         sysPrint("ReadRec() 5: The number of tokens is " + tokens.countTokens()) ;
         counter = 2;
         while (counter < 7) {
            recordTokens[counter] = tokens.nextToken().toString();
            counter++;
         }
         toolType  = new String(recordTokens[2]);
         brandName = new String(recordTokens[3]);
         partNum   = new String(recordTokens[4]);
         cost      = new String(recordTokens[5]);
         toolDesc  = new String(recordTokens[6]);
      } else {
         sysPrint("ReadRec() 6: There are no records to read.");
      }
   }

    /*
    ** The fill() method is used to fill in the passed string with blanks.
    */
    public StringBuffer fill (String str, StringBuffer buf, int len) {
       String str2 = new String("                     "  + "                                             " );
       if (str != null) {
          buf.setLength(len);
          buf = new StringBuffer(str + str2);
       } else {
          buf = new StringBuffer(str2);
       } 
       if (len == 0) {
          buf.setLength(45);
       } else {
          buf.setLength(len);
       }
      return buf;
   }

   /*
   ** write() Writes a record to the specified RandomAccessFile.
   */
   public void write(RandomAccessFile file) throws IOException {
      StringBuffer buf = new StringBuffer(" ");
      String str = "" , str2 = "";

      file.writeInt(recordId);
      file.writeInt(quantity);
      str = str + toolType + ";;" ;
      str = str + brandName + ";;";
      str = str + partNum + ";;";
      str = str + cost  + ";;";
      str = str + toolDesc + ";;;";
      buf = fill (str , buf.delete(0, 451), 451);
      file.writeChars(buf.toString());
      sysPrint("write(): - The value of recordId is " + recordId);
      sysPrint("write(): - The value of quantity is " + quantity);
      sysPrint("write(): - The value of str2 is " + str + " with a length of " + str2.length());
      sysPrint("write(): - The length of buf is " + buf.length());
   }

   public void writeInteger(RandomAccessFile file, int value) throws IOException {
      file.writeInt(value);
   }

   public int getrecordId() { 
      return recordId; 
   }

   public String getToolType() {
      return toolType.trim();
   }

   public String getToolDesc() { 
      return toolDesc.trim(); 
   }

   public String getPartNumber() { 
      return partNum.trim(); 
   }

   public int getQuantity() { 
      return quantity; 
   }

   public String getBrandName() { 
      return brandName.trim(); 
   }

   public String getCost() { 
      return cost.trim(); 
   }

   public void setToolType(String str) { 
      toolType = str; 
   }

   public void setrecordId(int recId) { 
      recordId = recId; 
   }

   public void setCost(String str) { 
      cost = str; 
   }

   public void setBrandName(String str) { 
      brandName = str; 
   }

   public void setToolDesc(String str) { 
      toolDesc = str; 
   }

   public void setPartNumber(String str) { 
      partNum = str; 
   }

   public void setQuantity(int quantityValue) { 
      quantity = quantityValue; 
   }

   public void setFilePos(long cursorPosition) { 
      filePos = cursorPosition; 
   }

   public void setFileLen(long fileLen) { 
      fileLen = fileLen; 
   }

   public void sysPrint(String str) {
      if (debugState) {
         System.out.println(str);
      }
   }

   public static int getSize() { 
      return 585; 
   }
}
