public class MenuHandler implements ActionListener {
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == fileMenuItem) {
            /**The Exit menu Item was selected. */
            cleanup();
        } else if (e.getSource() == lmMenuItem) {
            sysPrint("The Lawn Mower menu Item was selected.\n");

            display("Lawn Mowers");
        } else if (e.getSource() == lmtMenuItem) {
            sysPrint("The Lawn Mower Tractor menu Item was selected.\n");

            display("Lawn Tractor Mowers");
        } else if (e.getSource() == hdMenuItem) {
            sysPrint("The Hand Drill Tools menu Item was selected.\n");

            display("Hand Drill Tools");
        } else if (e.getSource() == dpMenuItem) {
            sysPrint("The Drill Press Power Tools menu Item was selected.\n");

            display("Drill Press Power Tools");
        } else if (e.getSource() == csMenuItem) {
            sysPrint("The Circular Saws Tools menu Item was selected.\n");

            display("Circular Saws");
        } else if (e.getSource() == hamMenuItem) {
            sysPrint("The Hammer menu Item was selected.\n");

            display("Hammers");
        } else if (e.getSource() == tabMenuItem) {
            sysPrint("The Table Saws menu Item was selected.\n");

            display("Table Saws");
        } else if (e.getSource() == bandMenuItem) {
            sysPrint("The Band Saws menu Item was selected.\n");

            display("Band Saws");
        } else if (e.getSource() == sandMenuItem) {
            sysPrint("The Sanders menu Item was selected.\n");

            display("Sanders");
        } else if (e.getSource() == stapMenuItem) {
            sysPrint("The Staplers menu Item was selected.\n");

            display("Staplers");
        } else if (e.getSource() == wdvMenuItem) {
            sysPrint("The Wet-Dry Vacs menu Item was selected.\n");
        } else if (e.getSource() == sccMenuItem) {
            sysPrint("The Storage, Chests & Cabinets menu Item was selected.\n");
        } else if (e.getSource() == deleteMI) {
            sysPrint("The Delete Record Dialog was made visible.\n");
            deleteRec = new DeleteRec(hws, file, table, pData);
            deleteRec.setVisible(true);
        } else if (e.getSource() == addMI) {
            sysPrint("The Add menu Item was selected.\n");
            pWord.displayDialog("add");
        } else if (e.getSource() == updateMI) {
            sysPrint("The Update menu Item was selected.\n");
            update = new UpdateRec(hws, file, pData, -1);
            update.setVisible(true);
        } else if (e.getSource() == listAllMI) {
            sysPrint("The List All menu Item was selected.\n");
        } else if (e.getSource() == debugON) {
            myDebug = true;
            sysPrint("Debugging for this execution is turned on.\n");
        } else if (e.getSource() == debugOFF) {
            sysPrint("Debugging for this execution is turned off.\n");
            myDebug = false;
        } else if (e.getSource() == helpHWMI) {
            sysPrint("The Help menu Item was selected.\n");
            File hd = new File("HW_Tutorial.html");
            Runtime runtime2 = Runtime.getRuntime();
            String[] callAndArgs = {"c:\\Program Files\\Internet Explorer\\IEXPLORE.exe",
                    "" + hd.getAbsolutePath()};

            try {
                Process child = runtime2.exec(callAndArgs);
                child.waitFor();
                sysPrint("Process exit code is: " +
                        child.exitValue());
            } catch (IOException e2) {
                sysPrint(
                        "IOException starting process!");
            } catch (InterruptedException e3) {
                System.err.println(
                        "Interrupted waiting for process!");
            }
        } else if (e.getSource() == aboutHWMI) {
            sysPrint("The About menu Item was selected.\n");
            Runtime rt = Runtime.getRuntime();
            String[] callAndArgs = {"c:\\Program Files\\Internet Explorer\\IEXPLORE.exe",
                    "http://www.sumtotalz.com/TotalAppsWorks/ProgrammingResource.html"};
            try {
                Process child = rt.exec(callAndArgs);
                child.waitFor();
                sysPrint("Process exit code is: " +
                        child.exitValue());
            } catch (IOException e2) {
                System.err.println(
                        "IOException starting process!");
            } catch (InterruptedException e3) {
                System.err.println(
                        "Interrupted waiting for process!");
            }
        }
        String current = (String) e.getActionCommand();
    }
}