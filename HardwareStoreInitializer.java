import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.awt.event.*;

import Record;
import PassWord;
import MenuHandler;
import NewRec;
import UpdateRec;
import DeleteRec;
import WindowHandler;
import MouseClickedHandler;


import javax.swing.JOptionPane;
import javax.swing.JTable;
import java.util.*;
import java.lang.Runtime;

/**
 * **************************************************
 * File:  HardwareStore.java
 * <p>
 * This program reads a random access file sequentially,
 * updates data already written to the file, creates new
 * data to be placed in the file, and deletes data
 * already in the file.
 */
public class HardwareStoreInitializer extends JFrame implements ActionListener {

    private PassWord passwordPopup;
    private UpdateRec updatePopup;
    private NewRec newRecordPopup;
    private DeleteRec deleteRecordPopup;
    private Record dataRecord;
    private String pData[][] = new String[250][7]; // TODO: Refacto nom variable : à quoi ca sert ??? C'est quoi ces variables en dur ???
    private JMenuBar menuBar;
    private MenuHandler menuHandler = new MenuHandler();
    private JTable table;
    private RandomAccessFile file;
    private JButton updateButton,
            newButton,
            deleteButton,
            listButton,
            doneButton; // TODO: Revoir le "doneButton" ("Quit program")
    private JMenu fileMenu,
            viewMenu,
            optionsMenu,
            toolsMenu,
            helpMenu,
            aboutMenu;
    private JMenuItem fileMenuItem,
    //View Menu Item            // TODO: à refacto => Signification métier de chaque variable ???
    lmMenuItem,
            lmtMenuItem,
            hdMenuItem,
            dpMenuItem,
            hamMenuItem,
            csMenuItem,
            tabMenuItem,
            bandMenuItem,
            sandMenuItem,
            stapMenuItem,
            wdvMenuItem,
            sccMenuItem,
    // Options Menu Items
    deleteMenuItem,
            addMenuItem,
            updateMenuItem,
            listAllMenuItem,
    // Tools Menu Items
    debugON,
            debugOFF,
    //Help Menu Items
    helpHWMI,
    //About Menu Items
    aboutHWMI;
    // file from which data is read
    private File aFile;
    private JButton cancel,
            refresh;
    private JPanel buttonPanel;
    private HardwareStore hws;
    private boolean myDebug = false;
    private int numEntries = 0;
    private Container container;


    public HardwareStoreInitializer() {
        super("Hardware Store: Lawn Mower ");

        dataRecord = new Record();
        aFile = new File("lawnmower.dat");
        container = getContentPane();

        setupMenu();

        InitRecord("lawnmower.dat", lawnMower, 27);

        InitRecord("lawnTractor.dat", lawnTractor, 11);

        InitRecord("handDrill.dat", handDrill, 15);

        InitRecord("drillPress.dat", drillPress, 10);

        InitRecord("circularSaw.dat", circularSaw, 12);

        InitRecord("hammer.dat", hammer, 12);

        InitRecord("tableSaw.dat", tableSaw, 15);

        InitRecord("bandSaw.dat", bandSaw, 10);

        InitRecord("sanders.dat", sanders, 15);

        InitRecord("stapler.dat", stapler, 15);


        setup();

        addWindowListener(new WindowHandler(this));
        setSize(700, 400);
        setVisible(true);
    }


    /**
     * ***********************************************************
     * setupMenu() is used to create the
     * 1- Menu Bar
     * 2- Menu Items
     ********************************************************/
    public void setupMenu() {
        /** Create the menubar */
        menuBar = new JMenuBar();

        /** Add the menubar to the frame */
        setJMenuBar(menuBar);

        /** Create the File menu and add it to the menubar  */
        fileMenu = new JMenu("File");

        menuBar.add(fileMenu);
        /** Add the Exit menuitems */
        fileMenuItem = new JMenuItem("Exit");
        fileMenu.add(fileMenuItem);
        fileMenuItem.addActionListener(menuHandler);

        /** Create the View menu and add it to the menubar  */
        viewMenu = new JMenu("View");

        /** Add the Lawn Mower menuitems */
        lmMenuItem = new JMenuItem("Lawn Mowers");
        viewMenu.add(lmMenuItem);
        lmMenuItem.addActionListener(menuHandler);

        /** Add the Lawn Mower menuitems */
        lmtMenuItem = new JMenuItem("Lawn Mowing Tractors");
        viewMenu.add(lmtMenuItem);
        lmtMenuItem.addActionListener(menuHandler);

        /** Add the Hand Drills Tools menuitems */
        hdMenuItem = new JMenuItem("Hand Drills Tools");
        viewMenu.add(hdMenuItem);
        hdMenuItem.addActionListener(menuHandler);

        /** Add the Drill Press Power Tools menuitems */
        dpMenuItem = new JMenuItem("Drill Press Power Tools");
        viewMenu.add(dpMenuItem);
        dpMenuItem.addActionListener(menuHandler);

        /** Add the Circular Saws  menuitems */
        csMenuItem = new JMenuItem("Circular Saws");
        viewMenu.add(csMenuItem);
        csMenuItem.addActionListener(menuHandler);

        /** Add the Hammer menuitems */
        hamMenuItem = new JMenuItem("Hammers");
        viewMenu.add(hamMenuItem);
        hamMenuItem.addActionListener(menuHandler);

        /** Add the Table Saws menuitems */
        tabMenuItem = new JMenuItem("Table Saws");
        viewMenu.add(tabMenuItem);
        tabMenuItem.addActionListener(menuHandler);

        /** Add the Band Saws menuitems */
        bandMenuItem = new JMenuItem("Band Saws");
        viewMenu.add(bandMenuItem);
        bandMenuItem.addActionListener(menuHandler);

        /** Add the Sanders menuitems */
        sandMenuItem = new JMenuItem("Sanders");
        viewMenu.add(sandMenuItem);
        sandMenuItem.addActionListener(menuHandler);


        /** Add the Stapler menuitems */
        stapMenuItem = new JMenuItem("Staplers");
        viewMenu.add(stapMenuItem);
        stapMenuItem.addActionListener(menuHandler);

        /** Add the Wet-Dry Vacs menuitems */
        wdvMenuItem = new JMenuItem("Wet-Dry Vacs");
        viewMenu.add(wdvMenuItem);
        wdvMenuItem.addActionListener(menuHandler);

        /** Add the Storage, Chests & Cabinets menuitems */
        sccMenuItem = new JMenuItem("Storage, Chests & Cabinets");
        viewMenu.add(sccMenuItem);
        sccMenuItem.addActionListener(menuHandler);

        menuBar.add(viewMenu);
        /** Create the Options menu and add it to the menubar  */
        optionsMenu = new JMenu("Options");

        /** Add the List All menuitems */
        listAllMenuItem = new JMenuItem("List All");
        optionsMenu.add(listAllMenuItem);
        listAllMenuItem.addActionListener(menuHandler);
        optionsMenu.addSeparator();

        /** Add the Add menuitems */
        addMI = new JMenuItem("Add");
        optionsMenu.add(addMI);
        addMI.addActionListener(menuHandler);

        /** Add the Update menuitems */
        updateMenuItem = new JMenuItem("Update");
        optionsMenu.add(updateMenuItem);
        updateMenuItem.addActionListener(menuHandler);
        optionsMenu.addSeparator();

        /** Add the Delete menuitems */
        deleteMI = new JMenuItem("Delete");
        optionsMenu.add(deleteMI);
        deleteMI.addActionListener(menuHandler);

        menuBar.add(optionsMenu);

        /** Create the Tools menu and add it to the menubar  */
        toolsMenu = new JMenu("Tools");
        menuBar.add(toolsMenu);
        /** Add the Tools menuitems */
        debugON = new JMenuItem("Debug On");
        debugOFF = new JMenuItem("Debug Off");
        toolsMenu.add(debugON);
        toolsMenu.add(debugOFF);
        debugON.addActionListener(menuHandler);
        debugOFF.addActionListener(menuHandler);

        /** Create the Help menu and add it to the menubar  */
        helpMenu = new JMenu("Help");

        /** Add the Help HW Store menuitems */
        helpHWMI = new JMenuItem("Help on HW Store");
        helpMenu.add(helpHWMI);
        helpHWMI.addActionListener(menuHandler);

        menuBar.add(helpMenu);

        /** Create the About menu and add it to the menubar  */
        aboutMenu = new JMenu("About");

        /** Add the About Store menuitems */
        aboutHWMI = new JMenuItem("About HW Store");
        aboutMenu.add(aboutHWMI);
        aboutHWMI.addActionListener(menuHandler);

        menuBar.add(aboutMenu);
    }

    /**
     * *******************************************************
     * Method: setup() is used to
     * 1- Open the lawnmower.dat file
     * 2- Call the toArray() method to popualte the JTable with
     * the contents of the lawnmower.dat file.
     * <p>
     * Called by the HardwareStore() constructor
     ********************************************************/
    public void setup() {
        double loopLimit = 0.0;
        int ii = 0, iii = 0;
        dataRecord = new Record();

        try {
            /** Divide  the length of the file by the record size to
             *  determine the number of records in the file
             */

            file = new RandomAccessFile("lawnmower.dat", "rw");

            aFile = new File("lawnmower.dat");

            numEntries = toArray(file, pData);

            file.close();
        } catch (IOException ex) {
            //part.setText( "Error reading file" );
        }

        /** ****************************************************************
         * pData contains the data to be loaded into the JTable.
         * columnNames contains the column names for the table.
         * 1- Create a new JTable.
         * 2- Add a mouse listener to the table.
         * 3- Make the table cells editable.
         * 4- Add the table to the Frame's center using the Border Layout
         *    Manager.
         * 5- Add a scrollpane to the table.
         *****************************************************************/
        table = new JTable(pData, columnNames);
        table.addMouseListener(new MouseClickedHandler(file, table, pData));
        table.setEnabled(true);

        container.add(table, BorderLayout.CENTER);
        container.add(new JScrollPane(table));
        /** Make the Frame visiable */
        cancel = new JButton("Cancel");
        refresh = new JButton("Refresh");
        buttonPanel = new JPanel();
        //buttonPanel.add( refresh ) ;
        buttonPanel.add(cancel);
        container.add(buttonPanel, BorderLayout.SOUTH);

        refresh.addActionListener(this);
        cancel.addActionListener(this);

        /** Create dialog boxes */
        updatePopup = new UpdateRec(hws, file, pData, -1);
        deleteRecordPopup = new DeleteRec(hws, file, table, pData);
        /** Allocate pWord last; otherwise  the update,
         *  newRec and deleteRec references will be null
         *  when the PassWrod class attempts to use them.*/
        passwordPopup = new PassWord(this);
    }

    /**
     * ***************************************************************
     * Method; InitRecord() is used to create and initialize the
     * tables used by the Hardware Store application. The parameters
     * passed are:
     * 1- String fileDat: is the name of the file to be created and
     * initialized.
     * 2- String FileRecord[][]: is the two dimensional array that contains
     * the initial data.
     * 3- int loopCtl: is the number of entries in the array.
     * <p>
     * Called by the HardwareStore() constructor
     ******************************************************************/
    public void InitRecord(String fileDat, String FileRecord[][],
                           int loopCtl) {

        aFile = new File(fileDat);

        sysPrint("initRecord(): 1a - the value of fileData is " + aFile);

        try {
            /** Open the fileDat file in RW mode.
             *  If the file does not exist, create it
             *  and initialize it to 250 empty records.
             */

            sysPrint("initTire(): 1ab - checking to see if " + aFile + " exist.");
            if (!aFile.exists()) {

                sysPrint("initTire(): 1b - " + aFile + " does not exist.");

                file = new RandomAccessFile(aFile, "rw");
                dataRecord = new Record();

                for (int ii = 0; ii < loopCtl; ii++) {
                    dataRecord.setRecID(Integer.parseInt(FileRecord[ii][0]));
                    sysPrint("initTire(): 1c - The value of record ID is " + dataRecord.getRecID());
                    dataRecord.setToolType(FileRecord[ii][1]);
                    sysPrint("initTire(): 1cb - The length of ToolType is " + dataRecord.getToolType().length());
                    dataRecord.setBrandName(FileRecord[ii][2]);
                    dataRecord.setToolDesc(FileRecord[ii][3]);
                    sysPrint("initTire(): 1cc - The length of ToolDesc is " + dataRecord.getToolDesc().length());
                    dataRecord.setPartNumber(FileRecord[ii][4]);
                    dataRecord.setQuantity(Integer.parseInt(FileRecord[ii][5]));
                    dataRecord.setCost(FileRecord[ii][6]);

                    sysPrint("initTire(): 1d - Calling Record method write() during initialization. " + ii);
                    file.seek(ii * Record.getSize());
                    dataRecord.write(file);

                }
            } else {
                sysPrint("initTire(): 1e - " + fileDat + " exists.");
                file = new RandomAccessFile(aFile, "rw");
            }

            file.close();
        } catch (IOException e) {
            System.err.println("InitRecord() " + e.toString() +
                    " " + aFile);
            System.exit(1);
        }
    }

    /**
     * ************************************************************
     * Method: display() is used to display the contents of the
     * specified table in the passed parameter. This method
     * uses the passed parameter to determine
     * 1- Which table to display
     * 2- Whether the table exists
     * 3- If it exists, the table is opened and its
     * contents are displayed in a JTable.
     * <p>
     * Called from the actionPerformed() method of the MenuHandler class
     *********************************************************************/
    public void display(String str) {

        String df = null, title = null;

        if (str.equals("Lawn Mowers")) {
            df = new String("lawnmower.dat");
            aFile = new File("lawnmower.dat");
            title = new String("Hardware Store: Lawn Mowers");
        } else if (str.equals("Lawn Tractor Mowers")) {
            df = new String("lawnTractor.dat");
            aFile = new File("lawnTractor.dat");
            title = new String("Hardware Store: Lawn Tractor Mowers");
        } else if (str.equals("Hand Drill Tools")) {
            df = new String("handDrill.dat");
            aFile = new File("handDrill.dat");
            title = new String("Hardware Store:  Hand Drill Tools");
        } else if (str.equals("Drill Press Power Tools")) {
            df = new String("drillPress.dat");
            aFile = new File("drillPress.dat");
            title = new String("Hardware Store: Drill Press Power Tools");
        } else if (str.equals("Circular Saws")) {
            df = new String("circularSaw.dat");
            aFile = new File("circularSaw.dat");
            title = new String("Hardware Store: Circular Saws");
        } else if (str.equals("Hammers")) {
            df = new String("hammer.dat");
            aFile = new File("hammer.dat");
            title = new String("Hardware Store: Hammers");
        } else if (str.equals("Table Saws")) {
            df = new String("tableSaw.dat");
            aFile = new File("tableSaw.dat");
            title = new String("Hardware Store: Table Saws");
        } else if (str.equals("Band Saws")) {
            df = new String("bandSaw.dat");
            aFile = new File("bandSaw.dat");
            title = new String("Hardware Store: Band Saws");
        } else if (str.equals("Sanders")) {
            df = new String("sanders.dat");
            aFile = new File("sanders.dat");
            title = new String("Hardware Store: Sanders");
        } else if (str.equals("Staplers")) {
            df = new String("stapler.dat");
            aFile = new File("stapler.dat");
            title = new String("Hardware Store: Staplers");
        }

        try {
            /** Open the .dat file in RW mode.
             *  If the file does not exist, create it
             *  and initialize it to 250 empty records.
             */

            sysPrint("display(): 1a - checking to see if " + df + " exists.");
            if (!aFile.exists()) {

                sysPrint("display(): 1b - " + df + " does not exist.");

            } else {
                file = new RandomAccessFile(df, "rw");

                this.setTitle(title);

                Redisplay(file, pData);
            }

            file.close();
        } catch (IOException e) {
            System.err.println(e.toString());
            System.err.println("Failed in opening " + df);
            System.exit(1);
        }

    }

    /**
     * *******************************************************
     * Method: Redisplay() is used to redisplay/repopualte the
     * JTable.
     * <p>
     * Called from the
     * 1- display() method
     * 2- actionPerformed() method of the UpdateRec class
     * 3- actionPerformed() method of the DeleteRec class
     ********************************************************/
    public void Redisplay(RandomAccessFile file, String a[][]) {


        for (int ii = 0; ii < numEntries + 5; ii++) {
            a[ii][0] = "";
            a[ii][1] = "";
            a[ii][2] = "";
            a[ii][3] = "";
            a[ii][4] = "";
            a[ii][5] = "";
            a[ii][6] = "";
        }
        int entries = toArray(file, a);
        sysPrint("Redisplay(): 1  - The number of entries is " + entries);
        setEntries(entries);
        container.remove(table);
        table = new JTable(a, columnNames);
        table.setEnabled(true);
        container.add(table, BorderLayout.CENTER);
        container.add(new JScrollPane(table));
        container.validate();
    }

    /**
     * ******************************************************************
     * Method: actionPerformed() - This is the event handler that responds
     * to the the cancel button  on the main frame.
     *********************************************************************/
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == refresh) {
            sysPrint("\nThe Refresh button was pressed. ");
            Container cc = getContentPane();

            table = new JTable(pData, columnNames);
            cc.validate();
        } else if (e.getSource() == cancel)
            cleanup();
    }

    /**
     * ***************************************************************
     * Method: cleanup() -This is the cleanup method that is used to close
     * the hardware.dat file and exit the application.
     * <p>
     * Called from the actionPerformed() method
     *********************************************************************/
    public void cleanup() {
        try {
            file.close();
        } catch (IOException e) {
            System.exit(1);
        }

        setVisible(false);
        System.exit(0);
    }

    /**
     * **************************************************************
     * Method: displayDeleteDialog()
     * <p>
     * Called from the actionPerformed() method of the PassWord class
     ******************************************************************/
    public void displayDeleteDialog() {
        sysPrint("The Delete Record Dialog was made visible.\n");
        deleteRecordPopup.setVisible(true);
    }

    /**
     * *******************************************************
     * Method: displayUpdateDialog()
     * <p>
     * Called from the actionPerformed() method of the PassWord class
     ******************************************************************/
    public void displayUpdateDialog() {
        sysPrint("The Update Record Dialog was made visible.\n");
        JOptionPane.showMessageDialog(null,
                "Enter the record ID to be updated and press enter.",
                "Update Record", JOptionPane.INFORMATION_MESSAGE);
        updatePopup = new UpdateRec(hws, file, pData, -1);
        updatePopup.setVisible(true);
    }

    /**
     * *******************************************************
     * Method: displayAddDialog()
     * <p>
     * Called from the actionPerformed() method of the PassWord class
     ******************************************************************/
    public void displayAddDialog() {
        sysPrint("The New/Add Record Dialog was made visible.\n");
        newRecordPopup = new NewRec(hws, file, table, pData);
        newRecordPopup.setVisible(true);
    }

    /**
     * *******************************************************
     * Method: setEntries() is called to set the number of current
     * entries in the pData array.
     ********************************************************/
    public void setEntries(int ent) {
        numEntries = ent;
    }

    /**
     * *******************************************************
     * Method: getPData() returns a specific row and column
     * <p>
     * This method is no longer used
     ********************************************************/
    public String getPData(int ii, int iii) {
        return pData[ii][iii];
    }

    /**
     * *******************************************************
     * Method: getEntries() returns the number of current entries
     * in the pData array.
     * <p>
     * Called from
     * 1- actionPerformed() method of the NewRec class
     * 2- actionPerformed() method of the DeleteRec class
     ********************************************************/
    public int getEntries() {
        return numEntries;
    }

    /**
     * *******************************************************
     * Method: sysPrint() is a debugging aid that is used to print
     * information to the screen.
     ********************************************************/
    public void sysPrint(String str) {
        if (myDebug) {
            System.out.println(str);
        }
    }

    /**
     * ***************************************************************
     * Method: toArray(RandomAccessFile lFile, String a[][])
     * <p>
     * Purpose: Returns an array containing all of the
     * elements in this list in the correct
     * order.
     * <p>
     * Called from the
     * 1- Setup method of the HardwareStore class
     * 2- Redisplay() method
     * *****************************************************************
     */
    public int toArray(RandomAccessFile file, String a[][]) {

        Record NodeRef = new Record(),
                PreviousNode = null;
        int ii = 0, iii = 0, fileSize = 0;

        try {
            fileSize = (int) file.length() / Record.getSize();
            sysPrint("toArray(): 1 - The size of the file is " + fileSize);
            /** If the file is empty, do nothing.  */
            if (fileSize > numEntries) {

                /** *************************************
                 *
                 * *************************************** */

                NodeRef.setFileLen(file.length());


                while (ii < fileSize) {
                    sysPrint("toArray(): 2 - NodeRef.getRecID is "
                            + NodeRef.getRecID());

                    file.seek(0);
                    file.seek(ii * NodeRef.getSize());
                    NodeRef.setFilePos(ii * NodeRef.getSize());
                    sysPrint("toArray(): 3 - input data file - Read record " + ii);
                    NodeRef.ReadRec(file);

                    String str2 = a[ii][0];
                    sysPrint("toArray(): 4 - the value of a[ ii ] [ 0 ] is " +
                            a[0][0]);

                    if (NodeRef.getRecID() != -1) {
                        a[iii][0] = String.valueOf(NodeRef.getRecID());
                        a[iii][1] = NodeRef.getToolType().trim();
                        a[iii][2] = NodeRef.getBrandName().trim();
                        a[iii][3] = NodeRef.getToolDesc().trim();
                        a[iii][4] = NodeRef.getPartNumber().trim();
                        a[iii][5] = String.valueOf(NodeRef.getQuantity());
                        a[iii][6] = NodeRef.getCost().trim();

                        sysPrint("toArray(): 5 - 0- " + a[iii][0] +
                                " 1- " + a[iii][1] +
                                " 2- " + a[iii][2] +
                                " 3- " + a[iii][3] +
                                " 4- " + a[iii][4] +
                                " 5- " + a[iii][5] +
                                " 6- " + a[iii][6]);

                        iii++;

                    } else {
                        sysPrint("toArray(): 5a the record ID is " + ii);
                    }

                    ii++;

                }  /** End of do-while loop   */
            }  /** End of outer if   */
        } catch (IOException ex) {
            sysPrint("toArray(): 6 - input data file failure. Index is " + ii
                    + "\nFilesize is " + fileSize);
        }

        return ii;

    }

}

